'''
getinchi

Returns the InChI strings for substances given in a file from PubChem,
if possible. There has to be one substance per line.

Usage:
  getinchi <filename> [-o <outfile>]

Arguments:
  -o <outfile>   write output to file <outfile>.

'''

import sys
import pubchempy as pcp
from docopt import docopt


def getinchi(name):
    print(f'Searching for substance: {name}', end='', flush=True)
    res = pcp.get_compounds(name, 'name')
    if len(res) == 0:
        print(f'  --  ERROR: not found in PubChem')
        return '[not found]'
    elif len(res) > 1:
        print(f'  --  ERROR: Has multiple entries in PubChem')
        return str(res)
    else:
        print(' --  FOUND')
        return res[0].inchi.strip()

def read_substances(fn):
    try:
        with open(fn, 'r') as f:
            substances = f.readlines()
    except:
        sys.exit(f'Could not read file {fn}')
    else:
        return [s.strip() for s in substances if s[0] != '#']

def main():
    args = docopt(__doc__, argv=sys.argv[1:], help=True)
    fn = args['<filename>']
    substances = read_substances(fn)
    result = ''
    for s in substances:
        inchi = getinchi(s)
        result += f'{s} ({inchi})\n'
    if args['-o']:
        with open(args['-o'], 'w') as f:
            f.writelines(result)
    else:
        print(result)
